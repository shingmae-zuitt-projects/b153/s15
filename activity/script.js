function oddEvenChecker(num1){
    if (typeof num1 === "number"){
        if (num1 % 2 === 0){
            console.log("The number is even.");
        } else if (num1 % 2 == 1) {
           console.log("The number is odd"); 
        } else {
            console.log("Invalid Input");
        }
    }
};

oddEvenChecker(12312342347);



function budgetChecker(A){
    if (typeof A === "number"){
        if (A > 4000) {
            console.log("You are over the budget.");
        }
        else if (A < 4000){
            console.log("You have resources left.");
        }
        else{
            console.log("Invalid Input");
        }
    }
};


budgetChecker(49786349);